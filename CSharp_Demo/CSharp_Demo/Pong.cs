﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BongChuyen
{
    class Pong
    {
        int width;
        int height;
        Board board;
        Paddle paddle1, paddle2;
        ConsoleKeyInfo keyinfo;
        ConsoleKey consoleKey;
        public Pong(int width, int height)
        {
            this.width = width; 
            this.height = height;
            board = new Board(height, width);
        }
        public void setUp()
        {
            paddle1 = new Paddle(2, height);
            paddle2 = new Paddle(width - 2, height);
            keyinfo = new ConsoleKeyInfo();
            consoleKey = new ConsoleKey();
        }
        void Input()
        {
            if (Console.KeyAvailable)
            {
                keyinfo = Console.ReadKey(true);
                consoleKey = keyinfo.Key;
            }
        }
        //Chay Chuong Trinh
        public void Run()
        {
            while (true)
            {
                Console.Clear();
                setUp();
                board.Write();
                paddle1.Write();
                paddle2.Write();
                while (true)
                {
                    Input();
                    switch (consoleKey)
                    {
                        case ConsoleKey.W:
                            paddle1.UP();
                            break;
                        case ConsoleKey.S:
                            paddle1.DOWN();
                            break;
                        case ConsoleKey.NumPad8:
                            paddle2.UP();
                            break;
                        case ConsoleKey.NumPad2:
                            paddle2.DOWN();
                            break;
                    }
                    consoleKey = ConsoleKey.A;
                    Thread.Sleep(100);
                }
            }
        }
    }

    class Paddle
    {
        public int X { set; get; }
        public int Y { set; get; }
        public int Length { set; get; }
        int boardHeight ;

        public Paddle(int x, int boardHeight)
        {
            this.boardHeight = 3;
            X = x;
            Y = boardHeight / 2;
            Length = boardHeight / 3;
        }
        public void UP()
        {
            if ((Y - 1 - (Length / 2)) != 0)
            {
                Console.SetCursorPosition(X, (Y + (Length / 2)) - 1);
                Console.Write("\0");
                Y--;
                Write();
            }
        }
        public void DOWN()
        {
            if ((Y + 1 + (Length / 2)) != boardHeight+2)
            {
                Console.SetCursorPosition(X, (Y - (Length / 2)));
                Console.Write("\0");
                Y++;
                Write();
            }
        }
        public void Write()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            for(int i = (Y - (Length / 2)); i < (Y + (Length / 2)); i++)
            {
                Console.SetCursorPosition(X, i);
                Console.Write("☺");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }

    // ve Board 
    public class Board
    {
        public int Height { set; get; }
        public int Width { set; get; }
        public Board()
        {
            Height = 20;
            Width = 60;
        }
        public Board (int height , int width)
        {
            Width = width;
            Height = height;
        }
        // ve Board
        public void Write() 
        {
            for( int i = 1; i <= Width; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write("♦");
            }
            for(int i = 1; i <= Width;i++)
            {
                Console.SetCursorPosition(i, (Height+1));
                Console.Write("♦");
            }
            for(int i = 1; i <= Height; i++)
            {
                Console.SetCursorPosition(0,i);
                Console.Write("♦");
            }
            for(int i = 1; i <= Height; i++)
            {
                Console.SetCursorPosition((Width + 1),i);
                Console.Write("♦");
            }
            // 4 goc vuong
            Console.SetCursorPosition(0, 0);
            Console.Write("♦");
            Console.SetCursorPosition(Width + 1, 0);
            Console.Write("♦");
            Console.SetCursorPosition(0, Height + 1);
            Console.Write("♦");
            Console.SetCursorPosition(Width + 1, Height + 1);
            Console.Write("♦");
        }
    }
}
